;; -*- mode: fennel; -*- vi:ft=fennel
;; Configuration file for Fenneldoc v0.1.7
;; https://gitlab.com/andreyorst/fenneldoc

(local {: major : minor : patch}
  (require :tasks.version))

{:fennel-path {}
 :function-signatures true
 :ignored-args-patterns ["%.%.%." "%_" "c" "s" "x" "f" "s[0-9]+" "ss" "y" "zs"]
 :inline-references "link"
 :insert-comment true
 :skip-undocumented? true
 :insert-copyright true
 :insert-license true
 :insert-version true
 :mode "checkdoc"
 :order "alphabetic"
 :out-dir "./doc"
 :project-copyright "Copyright (C) 2021 Andrey Listopadov"
 :modules-info
 {:src/io/gitlab/andreyorst/lazy-seq.fnl
  {:name "lazy-seq.fnl"
   :description
   "Lazy sequence library for Fennel and Lua.

Most functions in this library return a so called lazy sequence.  The
contents of such sequences aren't computed until requested, and
similarly to iterators, lazy sequences can be infinite.

The key difference from iterators is that sequence itself is a data
structure.  It can be passed, and shared between functions, and
operations on a sequence will not affect other callers.  Infinite
sequences are either consumed on per element basis, or bade finite by
calling `take` with desired size argument.

Both eager and lazy sequences support `pairs` iteration, which will
never terminate in case of infinite lazy sequence.  Such iterator
returns current sequence tail and it's head element as values.

Lazy sequences can also be created with the help of macros `lazy-seq`
and `lazy-cat`.  These macros are provided for convenience only."
   :doc-order ["cons"
               "first"
               "rest"
               "next"
               "seq"
               "lazy-seq"
               "list"
               "dorun"
               "doall"
               "realized?"
               "pack"
               "unpack"]}}
 :project-license "[MIT](https://gitlab.com/andreyorst/lazy-seq/-/raw/master/LICENSE)"
 :project-version (string.format "%s.%s.%s" major minor patch)
 :sandbox false
 :test-requirements {:src/io/gitlab/andreyorst/lazy-seq.fnl
                     "(require-macros (doto :io.gitlab.andreyorst.fennel-testb require))"}
 :toc true}
