(require-macros :io.gitlab.andreyorst.fennel-test)

(import-macros {: lazy-seq : lazy-cat}
  :io.gitlab.andreyorst.lazy-seq)

(local {: cons : rest : take : map : drop : first : range}
  (require :io.gitlab.andreyorst.lazy-seq))

(deftest fibbonacci-test
  (testing "recrusive lazy-seq"
    (let [fib ((fn fib [a b] (lazy-seq (cons a (fib b (+ a b))))) 0 1)]
      (assert-eq [0 1 1 2 3 5]
                 (take 6 fib))))
  (testing "arbitrary precision numbers test"
    (case (pcall require :bc)
      (true bc)
      (let [fib ((fn fib [a b]
                   (lazy-seq (cons a (fib b (+ a b)))))
                 (bc.new 0)
                 (bc.new 1))]
        (assert-eq
         (bc.new (.. "4106158863079712603335683787192671052201251086373692524088854309269055"
                     "8427411340373133049166085004456083003683570694227458856936214547650267"
                     "4373045446852160486606292497360503469773453733196887405847255290082049"
                     "0869075126220590545421958897580311092226708492747938595391333183712447"
                     "9554314761107327624006673793408519173181099320170677683893476676477873"
                     "9502174470268627820918553842225858306408301661862900358266857238210235"
                     "8025043519514729979196765240047842363764533472683641526483462458405732"
                     "1424141993791724291860263981009786694239201540462015381867142573983507"
                     "4851396421139982713640679581178458198658692285968043243656709796000"))
         (first (drop 3000 fib))))
      (_ err) (io.stderr:write "Skipping arbitrary precision numbers test: " err "\n"))))

(deftest lazy-cat-test
  (testing "lazy range concatenation"
    (let [r (lazy-cat (take 10 (range)) (drop 10 (range)))]
      (assert-eq [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14]
                 (take 15 r))))
  (testing "lazy-cat global fibbonacci test"
    (global fib* (lazy-cat [0 1] (map #(+ $1 $2) (rest fib*) fib*)))
    (assert-eq [0 1 1 2 3 5 8]
               (take 7 fib*))))
